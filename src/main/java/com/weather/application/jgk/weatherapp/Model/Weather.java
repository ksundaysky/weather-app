package com.weather.application.jgk.weatherapp.Model;

import java.sql.Timestamp;

public class Weather extends WeatherEntry {

	public Weather(String timestamp, double temperature, Integer weatherId, String weatherIcon, String cityName) {
		super(timestamp, temperature, weatherId, weatherIcon, cityName);
	}

	public Weather()
	{
		super(null,1.0,null,null,null);
	}

	private String name;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
