package com.weather.application.jgk.weatherapp.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Entity
@Table(name = "weather")
public class WeatherEntry implements Serializable {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")

	private int id;

	@Column(name = "timestamp")
	private String timestamp;

	@Column(name = "temperature")
	private double temperature;

	@Column(name = "weatherId")
	private Integer weatherId;

	@Column(name = "weatherIcon")
	private String weatherIcon;

	@Column(name = "cityName")
	private  String cityName;


	public WeatherEntry(String timestamp, double temperature, Integer weatherId, String weatherIcon, String cityName) {
		this.timestamp = timestamp;
		this.temperature = temperature;
		this.weatherId = weatherId;
		this.weatherIcon = weatherIcon;
		this.cityName = cityName;
	}

	public WeatherEntry() {
	}

	@JsonProperty("timestamp")
	public String getTimestamp() {
		return this.timestamp;
	}

	@JsonSetter("dt")
	public void setTimestamp(long unixTime) {

// convert seconds to milliseconds
		Date date = new java.util.Date(unixTime*1000L);
// the format of your date
		SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
// give a timezone reference for formatting (see comment at the bottom)
		//sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT"));
		String formattedDate = sdf.format(date);
		System.out.println(formattedDate);
		this.timestamp = formattedDate;
	}

	/**
	 * Return the temperature in Kelvin (K).
	 */
	public double getTemperature() {
		return this.temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	@JsonProperty("main")
	public void setMain(Map<String, Object> main) {
		setTemperature(Double.parseDouble(main.get("temp").toString()));
	}

	public Integer getWeatherId() {
		return this.weatherId;
	}

	public void setWeatherId(Integer weatherId) {
		this.weatherId = weatherId;
	}

	public String getWeatherIcon() {
		return this.weatherIcon;
	}

	public void setWeatherIcon(String weatherIcon) {
		this.weatherIcon = weatherIcon;
	}

	@JsonProperty("weather")
	public void setWeather(List<Map<String, Object>> weatherEntries) {
		Map<String, Object> weather = weatherEntries.get(0);
		setWeatherId((Integer) weather.get("id"));
		setWeatherIcon((String) weather.get("icon"));
	}

	public String getCityName() {
		return cityName;
	}

	@JsonProperty("name")
	public void setCityName(String name) {
		this.cityName = name;
	}

	@Override
	public String toString() {
		return "WeatherEntry{" +
				"id=" + id +
				", timestamp=" + timestamp +
				", temperature=" + temperature +
				", weatherId=" + weatherId +
				", weatherIcon='" + weatherIcon + '\'' +
				", cityName='" + cityName + '\'' +
				'}';
	}
}
