package com.weather.application.jgk.weatherapp.Repository;

import com.weather.application.jgk.weatherapp.Model.WeatherEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//@Repository("weatherRepository")
public interface WeatherRepository extends JpaRepository<WeatherEntry,Long> {

}
