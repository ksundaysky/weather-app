package com.weather.application.jgk.weatherapp.Model;
import com.weather.application.jgk.weatherapp.WeatherAppProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;



import java.net.URI;

@Service
public class WeatherService {

	private static final String WEATHER_URL =
			"http://api.openweathermap.org/data/2.5/weather?q={city}&APPID={key}";
	private static final String WEATHER_COORDINATES_URL =
			"http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&APPID={key}";
	private static final String FORECAST_URL =
			"http://api.openweathermap.org/data/2.5/forecast?q={city},{country}&APPID={key}";

	private static final Logger logger = LoggerFactory.getLogger(WeatherService.class);

	private final RestTemplate restTemplate;

	private final String apiKey;

	public WeatherService(RestTemplateBuilder restTemplateBuilder,
			WeatherAppProperties properties) {
		this.restTemplate = restTemplateBuilder.build();
		this.apiKey = properties.getApi().getKey();
	}

//	@Cacheable("weather")
	public Weather getWeather( String city) {
		logger.info("Requesting current weather for {}",  city);
		URI url = new UriTemplate(WEATHER_URL).expand(city, this.apiKey);
		try {

			return invoke(url, Weather.class);

		}
		catch (HttpClientErrorException e)
		{
			System.out.println("dupa");
			return null;
		}
	}

	public Weather getWeatherCoordinates( String lat, String lon) {
		logger.info("Requesting current weather for  latitude {} longitude {}",  lat,lon);
		URI url = new UriTemplate(WEATHER_COORDINATES_URL).expand(lat,lon, this.apiKey);
		try {

			return invoke(url, Weather.class);

		}
		catch (HttpClientErrorException e)
		{
			System.out.println("dupa");
			return null;
		}
	}

//	@Cacheable("forecast")
	public WeatherForecast getWeatherForecast(String country, String city) {
		logger.info("Requesting weather forecast for {}/{}", country, city);
		URI url = new UriTemplate(FORECAST_URL).expand(city, country, this.apiKey);
		try {

			return invoke(url, WeatherForecast.class);

		}
		catch (HttpClientErrorException e)
		{
			System.out.println("dupa");
		}
		return null;
	}

	private <T> T invoke(URI url, Class<T> responseType) throws HttpClientErrorException {
		RequestEntity<?> request = RequestEntity.get(url)
				.accept(MediaType.APPLICATION_JSON).build();
		ResponseEntity<T> exchange = this.restTemplate
				.exchange(request, responseType)  ;
		//System.out.println(exchange);

		return exchange.getBody();
	}

}
