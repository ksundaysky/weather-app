package com.weather.application.jgk.weatherapp.Controller;

import com.weather.application.jgk.weatherapp.Model.*;
import com.weather.application.jgk.weatherapp.Repository.WeatherRepository;
import com.weather.application.jgk.weatherapp.WeatherAppProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/")
public class WeatherSummaryController {

	private final WeatherService weatherService;

	private final WeatherAppProperties properties;

	public String city;

	public String lon;
	public String lat;

	@Autowired
	WeatherRepository weatherRepository;


	@Autowired
	WeatherAppProperties weatherAppProperties;


	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	@RequestMapping(value = "/",method = RequestMethod.GET)
	public ModelAndView index() {

		//String city=new String("");
		ModelAndView modelAndView = new ModelAndView();


		CityName city = new CityName();
		modelAndView.addObject("city",city);
		modelAndView.setViewName("index");
		return modelAndView;
	}



	@RequestMapping("/latitude={latitude}&longitude={longitude}")
	public ModelAndView dupa(@PathVariable(name = "latitude") String latitude, @PathVariable(name="longitude") String longitude ) {


		System.out.println(latitude + "   " + longitude);

		this.lat=latitude;
		this.lon = longitude;

		Map<String, Object> model = new LinkedHashMap<>();
		Object summary = getSummaryCoordinates();
		if(summary==null)
		{
			return new ModelAndView("404");
		}
		model.put("summary", summary);
		return new ModelAndView("summary", model);
	}
//	@RequestMapping(method = RequestMethod.POST)
//	public ModelAndView post() {
//
//		//weatherAppProperties.setLocations();
//		return new ModelAndView("index");
//	}
	public WeatherSummaryController(WeatherService weatherService, WeatherAppProperties properties) {
		this.weatherService = weatherService;
		this.properties = properties;
}

	@RequestMapping(value = "/",method = RequestMethod.POST)
	public ModelAndView index(@Valid @ModelAttribute("city") CityName city) {

		//weatherAppProperties.setLocations(Arrays.asList(city));
		this.city=city.name;
		System.out.println(city.name);
		Map<String, Object> model = new LinkedHashMap<>();
		Object summary = getSummary();
		if(summary==null)
		{
			return new ModelAndView("404");
		}
		model.put("summary", summary);
		return new ModelAndView("summary", model);


	}

	private Object getSummary() {
		//List<WeatherSummary> summary = new ArrayList<>();
		WeatherSummary summary;

			Weather weather = this.weatherService.getWeather( this.city);
			if(weather==null)
			{
				return null;
			}

			WeatherEntry weatherEntry = new WeatherEntry(weather.getTimestamp(),weather.getTemperature(),weather.getWeatherId(),weather.getWeatherIcon(),weather.getCityName());
			weatherRepository.save(weatherEntry);

			summary = createWeatherSummary( weather);
		//}
		return summary;
	}

	private Object getSummaryCoordinates() {

		WeatherSummary summary;
		Weather weather = this.weatherService.getWeatherCoordinates(this.lat, this.lon);

		if(weather==null)
		{
			return null;
		}

		System.out.println("getSummaryCoordinates");
		System.out.println(weather.toString());

		WeatherEntry weatherEntry = new WeatherEntry(weather.getTimestamp(),weather.getTemperature(),weather.getWeatherId(),weather.getWeatherIcon(),weather.getCityName());
		weatherRepository.save(weatherEntry);
		summary = createWeatherSummary(weather);
		return summary;
	}

	private WeatherSummary createWeatherSummary(
			Weather weather) {

		return new WeatherSummary(weather);
	}

}
